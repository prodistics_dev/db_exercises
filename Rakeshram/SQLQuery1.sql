USE [Bank]
GO

DROP TABLE IF EXISTS [dbo].[Transactions]
GO

DROP TABLE IF EXISTS [dbo].[Accounts]
GO
 
DROP TABLE IF EXISTS [dbo].[Customers]
GO

CREATE TABLE [dbo].[Customers](
	[Id] [int] IDENTITY (1,1) PRIMARY KEY,
	[Name] [varchar](50) NULL,
	[Phonenumber] [bigint] NULL,
	[Email] [varchar](50) NULL
) ON  [PRIMARY]
GO

CREATE TABLE [dbo].[Accounts](
	[AccountNumber] [bigint] NULL,
	[AccountType] [varchar](50) NULL,
	[CustomerId] [int]NOT NULL
	CONSTRAINT FK_customers FOREIGN KEY (CustomerId)
	REFERENCES [dbo].[Customers] (Id)
) ON  [PRIMARY]
GO

CREATE TABLE [dbo].[Transactions](
	[TransactionAmount] [bigint] NULL,
	[CheckBalance] [bigint] NULL,
	[AccountId][int]NOT NULL
	CONSTRAINT FK_accounts FOREIGN KEY (AccountId)
	REFERENCES [dbo].[Customers] (Id)
)ON [PRIMARY]
GO

--Insert record into Customers table

INSERT INTO [dbo].[Customers](Name, Phonenumber,Email)
VALUES ('Arun',9952769123,'arunvumar@gmail.com');
GO

INSERT INTO [dbo].[Customers](Name, Phonenumber,Email)
VALUES ('kumar',8752762124,'kumarkumar@gmail.com');
GO

INSERT INTO [dbo].[Customers](Name, Phonenumber,Email)
VALUES ('Raj',7752769111,'rajmomo@yahoo.com');
GO

INSERT INTO [dbo].[Customers](Name, Phonenumber,Email)
VALUES ('Bala',9952769123,'xoxofire@gmail.com');
GO

INSERT INTO [dbo].[Customers](Name, Phonenumber,Email)
VALUES ('kdbila',9952769123,'arunkumar@gmail.com');
GO

SELECT * FROM [dbo].[Customers];
GO

--Insert records into Accounts table

INSERT INTO [dbo].[Accounts](AccountNumber, AccountType,CustomerId)
VALUES (2374913501345,'LoanAccount',1);
GO

INSERT INTO [dbo].[Accounts](AccountNumber, AccountType,CustomerId)
VALUES (5674913501311,'SavingsAccount',1);
GO

INSERT INTO [dbo].[Accounts](AccountNumber, AccountType,CustomerId)
VALUES (9974913501323,'CheckAccount',2);
GO

INSERT INTO [dbo].[Accounts](AccountNumber, AccountType,CustomerId)
VALUES (5555913501348,'LoanAccount',3);
GO

INSERT INTO [dbo].[Accounts](AccountNumber, AccountType,CustomerId)
VALUES (6664913501345,'SavingsAccount',4);
GO

SELECT * FROM [dbo].[Accounts];
GO


--Insert records into Transactions table

INSERT INTO [dbo].[Transactions](TransactionAmount,CheckBalance,AccountId)
VALUES (1000,55500,1);
GO

INSERT INTO [dbo].[Transactions](TransactionAmount,CheckBalance,AccountId)
VALUES (1500,75500,3);
GO

INSERT INTO [dbo].[Transactions](TransactionAmount,CheckBalance,AccountId)
VALUES (2000,8500,2);
GO

SELECT * FROM [dbo].[Transactions];
GO

DELETE FROM [dbo].[Customers] WHERE name='Kdbila'
GO

DELETE FROM [dbo].[Transactions] WHERE CheckBalance='8500'
GO

SELECT [dbo].[Customers].Name,[dbo].[Accounts].AccountNumber,[dbo].[Transactions].CheckBalance
FROM ((Customers
INNER JOIN Accounts ON Customers.Id = Accounts.CustomerId)
INNER JOIN Transactions ON Customers.Id = Transactions.AccountId);

-- JOINS 

SELECT * FROM Customers

SELECT * FROM Accounts

SELECT * FROM Customers
LEFT JOIN AccountsTest
ON Customers.Id = AccountsTest.CustomerId

CREATE TABLE [dbo].[AccountsTest](
	[AccountNumber] [bigint] NULL,
	[AccountType] [varchar](50) NULL,
	[CustomerId] [int]NOT NULL
) ON  [PRIMARY]
GO

INSERT INTO [dbo].[AccountsTest](AccountNumber, AccountType,CustomerId)
VALUES (2374913501345,'LoanAccount',1);
GO

INSERT INTO [dbo].[AccountsTest](AccountNumber, AccountType,CustomerId)
VALUES (5674913501311,'SavingsAccount',1);
GO

INSERT INTO [dbo].[AccountsTest](AccountNumber, AccountType,CustomerId)
VALUES (9974913501323,'CheckAccount',2);
GO

INSERT INTO [dbo].[AccountsTest](AccountNumber, AccountType,CustomerId)
VALUES (5555913501348,'LoanAccount',3);
GO

INSERT INTO [dbo].[AccountsTest](AccountNumber, AccountType,CustomerId)
VALUES (6664913501345,'SavingsAccount',4);
GO

INSERT INTO [dbo].[AccountsTest](AccountNumber, AccountType,CustomerId)
VALUES (6664913501345,'SavingsAccount',100);
GO