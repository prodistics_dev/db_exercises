--USE [master]
--GO

--DROP DATABASE[chennova]
--GO

--CREATE DATABASE [chennova]
--GO

USE [chennova]
GO

DROP TABLE [dbo].[employee]
GO

DROP TABLE [dbo].[dept]
GO

CREATE TABLE [dbo].[dept](
	[Id] [int] IDENTITY (1,1) PRIMARY KEY,
	[Name] [varchar](50) NULL,
	[Head] [varchar](50) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[employee](
	[Id] [int] IDENTITY (1,1) PRIMARY KEY,
	[name] [varchar](50) NULL,
	[Phonenumber] [bigint] NULL,
	[emailId] [varchar](50) NULL,
	[gender] [varchar](10) NULL,
	[deptId] [int] NOT NULL
	CONSTRAINT FK_Dept FOREIGN KEY (deptId)
    REFERENCES [dbo].[dept] (Id)
) ON [PRIMARY]
GO

--Insert record into dept table

INSERT INTO [dbo].[dept](Name, Head)
VALUES ('Finance','Karthi');
GO

INSERT INTO [dbo].[dept](Name, Head)
VALUES ('Marketing','Jagan');
GO

SELECT * FROM [dbo].[dept];
GO

--Insert record into employee table

INSERT INTO [dbo].[employee](Name, Phonenumber,emailId,gender,deptId)
VALUES ('Ram',9995552345,'tenten@gmail.com','male',1);
GO

INSERT INTO [dbo].[employee](Name, Phonenumber,emailId,gender,deptId)
VALUES ('Gopal',9993332345,'karaten@gmail.com','male',2);
GO

INSERT INTO [dbo].[employee](Name, Phonenumber,emailId,gender,deptId)
VALUES ('Kanjana',8893332345,'yetapaen@gmail.com','Female',2);
GO

INSERT INTO [dbo].[employee](Name, Phonenumber,emailId,gender,deptId)
VALUES ('Karan',8993332345,'karan@yahoo.com','male',2);
GO

SELECT * FROM [dbo].[employee];
GO

SELECT * FROM [dbo].[employee] WHERE deptId=2;
GO

UPDATE [dbo].[employee]
SET name= 'Janani'
WHERE Id=5
GO

DELETE FROM [dbo].[employee] WHERE name='Karan'
GO

SELECT [dbo].[dept].Name,[dbo].[employee].name,Phonenumber,emailId,gender
FROM [dbo].[employee]
LEFT JOIN [dbo].[dept]
ON [dbo].[employee].deptId =[dbo].[dept].Id;
GO





